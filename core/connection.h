//  ForBy LTD 2017
/// ConnTools v0.2f

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef FORBY_CONNECTION_T
#define FORBY_CONNECTION_T

#include <stdexcept>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cerrno>

namespace ForBy
{

struct Connection_T
{
    int fd;                             // File Descriptor for socket
    socklen_t addrlen;                  // addrlen def for sockaddr mutators

    struct sockaddr addr;

    inline ~Connection_T() noexcept
    {
       close(this->fd);
    }
};

static Connection_T
accept_client(const int& fd)
{
    Connection_T buff;
    if( ( buff.fd = accept(fd, &buff.addr, &buff.addrlen )) == -1 )
        throw std::runtime_error("Error while accepting connection request");

    return buff;
}

} // namespace ForBy


#endif // FORBY_CONNECTION_T

//  ForBy LTD 2017
/// ConnTools v0.3 ( since 0.2f )

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef FORBY_CONNTOOLS_ADDRTOOLS
#define FORBY_CONNTOOLS_ADDRTOOLS

#include <ForBy/ConnTools/core/connection.h>

#ifndef DEFAULT_PORT
  #define DEFAULT_PORT 0
#endif // DEFAULT_PORT

#ifndef DEFAULT_ADDRESS
  #define DEFAULT_ADDRESS "127.0.0.1"
#endif // DEFAULT_ADDRESS


namespace ForBy{
namespace ConnTools
{

/// Default Socket Protocols supported
enum sock_protocols
{
    RAW
,   TCP
,   UDP
,   SCTP
,   ICMP
};

struct addr_info
{
    int  ip_protocol  = AF_INET;               // 1 arg from socket() AFINET/AFINET6, etc
    int  sock_type    = SOCK_STREAM;           // 2 arg from socket();
    int  sock_flags   = 0;                     // 3 arg from socket();


    char* sock_address = DEFAULT_ADDRESS;      // Sock address
    unsigned int port  = DEFAULT_PORT;         // Socket Port

    struct sockaddr addr;                      // Constructed address allowing generic protocols
    socklen_t ai_addrlen = 0;                  // Address lenght specificator. Importan when defining Protocol at implementation
};

static inline addr_info
use_socket_protocol(const sock_protocols _prot) noexcept
{
    addr_info buffer;

    switch(_prot)
    {
        case(TCP):
        {
            buffer.sock_type = SOCK_STREAM;
            break;
        }
        case(UDP):
        {
            buffer.sock_type = SOCK_DGRAM;
            break;
        }
        case(SCTP):
        {
            buffer.sock_type  = SOCK_STREAM;
            buffer.sock_flags = IPPROTO_SCTP;
            break;
        }
        case(ICMP):
        {
            buffer.sock_type  = SOCK_RAW;
            buffer.sock_flags = IPPROTO_ICMP;
            break;
        }
        case(RAW):
        {
            buffer.sock_type  = SOCK_RAW;
            buffer.sock_flags = IPPROTO_RAW;
            break;
        }
    } // switch

    return buffer;
}

} // namespace ConnTools
} // Nampesace ForBy

#endif // FORBY_CONNTOOLS_ADDRTOOLS

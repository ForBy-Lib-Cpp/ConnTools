//  ForBy LTD 2017
//  ConnTools v0.2e

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef FORBY_CONNTOOLS_CONNBUFFER
#define FORBY_CONNTOOLS_CONNBUFFER

#include <atomic>
#include <utility>

#include <ForBy/ConnTools/core/Socket.hpp>

namespace ForBy {
namespace ConnTools
{

template< typename T = Connection_T>
struct Item_T
{
    Item_T( T&& _data )
    {
        this->data = std::move(_data);
        this->id = ++identor;

        DEBUG_LOG( LOG_INFO, "Instance initialized with id: " , id, " -> ", this, LOG_END);
    }

    T data;
    ulong id;
    epoll_event event;

    static std::atomic<unsigned long> identor;
};


template < typename T = Connection_T>
class SecureBuffer
{
public:

    using Item = template Item_T<T>;
    // Set dinamic Item to Item_T<typename T>

    SecureBuffer()      // Buffer constructor. Set events array as vectors objetcs
    {
        this->events = &container.front();
    }

    Item* add_item( Item item ) noexcept
    {
        Item* item_ptr;

        while( busy.test_and_set( std::memory_order_acquire ));

        container.push_back( std::move( item ));
        item_ptr = &container.back();

        busy.clear( std::memory_order_release );
        return item_ptr;
    }

    Item* add_items( Item *item, size_t s) noexcept
    {
        Item *item_ptr = nullptr;

        while( busy.test_and_set( std::memory_order_acquire ));

        for( s = (s-1) ; s > 0; s--)
            container.push_back( std::move( item[s] ));

        *item_ptr = &container.front();

        busy.clear( std::memory_order_release );
        return item_ptr;
    }

    void discard_item( Item &item ) noexcept
    {
        while( busy.test_and_set( std::memory_order_acquire ));


        for( size_t i = container.size()-1 ; i > 0 ; i-- )
            if( container.at(i).id == item.id )
            {
                container.erase( container.begin()+i );
                break;
            }

        busy.clear( std::memory_order_release );
    }

    size_t get_items( Item *item_ptr ) noexcept
    {
        size_t counter;

        while( busy.test_and_set( std::memory_order_acquire ));

        item_ptr = &container.front();
        counter  = container.size();

        busy.clear( std::memory_order_release );
        return counter;
    }

    Item* pump_items() noexcept
    {
        while( busy.test_and_set( std::memory_order_acquire ));
          size_t total = container.size();

          Item *items[ total ];

          for( size_t i = 0 ; i < total; i++)
          {
              items[i] = container.at(i);
              container.erase( container.begin()+i );
          }

        busy.clear( std::memory_order_release );

        return items;
    }

    size_t get_size() const noexcept
    {
        return container.size();
    }

    ~SecureBuffer(){};


private:

    std::atomic_flag busy = ATOMIC_FLAG_INIT;
    std::vector<Item> container;

};


} // Namespace Miscelaneous
} // Namespace ForBy


#endif // FORBY_CONNTOOLS_CONNBUFFER

//   ForBy LTD 2017
//   ConnTools
///  Socket Base Class

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */


#ifndef FORBY_CONNTOOLS_SOCKET_T
#define FORBY_CONNTOOLS_SOCKET_T

#define _forby_conntools_socket_edition "0.3.1"

#include <ForBy/ConnTools/core/SocketIO.hpp>
#include <ForBy/ConnTools/core/Tools.hpp>

namespace ForBy {
namespace ConnTools
{

  template< class Socket_IO >
  struct Socket_T
  :   virtual public Connection_T
  ,   virtual public Socket_IO
  {
      friend class Tools;
      virtual ~Socket_T(){}

      inline void stop() noexcept { close( fd ); }

      inline int& operator*() noexcept 
      {
          return this->fd;
      }
 };


}  // Namespace ConnTools

} // Namespace ForBy

#endif // FORBY_CONNTOOLS_SOCKET_T

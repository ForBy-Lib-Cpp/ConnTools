//  ForBy LTD 2017
/// ConnTools

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */


#ifndef FORBY_CONNTOOLS_SOCKET_IO
#define FORBY_CONNTOOLS_SOCKET_IO

#define _forby_conntools_socketio_edition 0x001b
#include <ForBy/ConnTools/core/connection.h>
#include <cstring>

namespace ForBy{
namespace ConnTools
{

  class SocketIO_T
  :  virtual public Connection_T
  {
  public:

      virtual void sender( const char* _msg ) = 0;
      virtual const char* receiver() = 0;
      virtual const char* meet( const char* _msg) = 0;

  protected:

      void __send( const char* _input) const
      {
          size_t _size = strlen(_input);

          if( send( fd, (char*)&_size, sizeof(_size), 0) < 0 )
          {
              throw std::runtime_error("Could not notify length of data");
              return;
          }

          for( size_t result = 0, pos = 0 ; ;)
          {
              if( (result = send( fd, &_input[pos], _size, 0)) < 0)
                  throw std::runtime_error("Could not send data throw Socket: "+result);

              else continue;

              if( ( _size -= result) == 0)
                 break;
              else
                 pos = _size;

              }
        }


      const char* __recv() const
      {
          size_t l = 0;

          if( recv(this->fd, (char*)&l, sizeof(l), 0) < 0 )
          {
              throw std::runtime_error("Could not get stream length");
              return NULL;
          }

          //Gen buffer array with size = l
          char buffer[ l + 1 ];

          if( int rr = recv(this->fd, &buffer[0], l, MSG_WAITALL) < 0)
            if( rr < 0)
            {
                throw std::runtime_error("Message reception could not complete");
                return NULL;
	    }
	    else if( rr > l)
	    {
     	        throw std::runtime_error("Buffer overflow");
                abort();
            }

          return buffer;
      }

      virtual ~SocketIO_T()
      {}

  };   // class SocketIO_T



} // namespace ConnTools
} // namespace ForBy


#endif // FORBY_CONNTOOLS_SOCKET_IO

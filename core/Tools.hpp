//     ForBy LTD 2018
//    ConnTools v0.3.1

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/// Socket_T Manipulator

#ifndef FORBY_CONNTOOLS_TOOLS_INCLUDED
#define FORBY_CONNTOOLS_TOOLS_INCLUDED

#include <ForBy/ConnTools/core/Socket.hpp>
#include <ForBy/ConnTools/core/AddrTools.hpp>
#include <netdb.h>

#include <ForBy/Logger/Logger.hpp>

#ifndef MAX_CONCURRENT
    #define MAX_CONCURRENT 5
#endif

namespace ForBy{
namespace ConnTools
{

class Tools
{

public:

    /* brief Bind socket to an address.
    *   Specialization for Socket_T of c bind() for socket fd
    *   args Connection_T*, struct addr_info*)
    */
    static inline void
    bind_socket( Connection_T* _socket, addr_info* _ai)
    {
        if( (_ai == nullptr) || (_ai->ai_addrlen == 0 ))
            throw std::logic_error("Empty addr_info or unconfigured address given");

        if( bind( _socket->fd, (struct sockaddr*)&_ai->addr, _ai->ai_addrlen) < 0 )
            throw std::runtime_error("Could not bind Socket to address struct");
    }

    /* brief Instanciate Socket_T object.
     * Gets and FD for it and configure its flags
     * args Connection_T*, addr_info*
     * return void
    */

    static inline void
    get_socket( Connection_T* _socket, addr_info* _ai)
    {
        if( ( _socket->fd = socket(_ai->ip_protocol, _ai->sock_type, NULL)) == -1 )
            throw std::runtime_error("Could not get file descriptor for socket");

        int flags;
        if
        (       ( flags = fcntl ( _socket->fd, F_GETFL, 0)) == -1
        ||      ( fcntl( _socket->fd, F_SETFL, (flags |= _ai->sock_flags)) ) == -1
        )
            throw std::runtime_error("Could not modify socket's Flags");
    }

    /* brief
     * Iniciates listening on a Socket_T using C listen() with steroids
     * args Connection_T*, int max_concurrents
     * return void
    */
    static inline void
    service_listen( Connection_T* _socket, int max_concurrent = MAX_CONCURRENT)
    {
        if( listen( _socket->fd, max_concurrent ) == -1 )
        {
            throw std::runtime_error("Could not start listening");
            return;
        }
    }


    /* brief
    *  Try to connect Connection_T to its address
    * args Connection_T*
    */
    static inline void
    try_connect(Connection_T* _src)
    {
        if( connect( _src->fd, &_src->addr, _src->addrlen ) == -1 )
            throw std::runtime_error( "Could not connect" );
    }

    /* brief
    * Gets ip_address of given addr_info
    * args addr_info&
    * return const char*
    */
    static inline const char*
    get_ip_address( const Connection_T* _src)
    {
        char hoststr[NI_MAXHOST];

        return
        getnameinfo
        (  &_src->addr
        ,  _src->addrlen
        ,  hoststr, sizeof(hoststr)
        ,  NULL, NULL
        ,  NI_NUMERICHOST
        ) == 0 ?
           hoststr
        :  nullptr
        ;

    }

   // Template function returns storager pointers

    static inline addr_info*
    get_sockaddr_in( addr_info& ai )
    {
        struct sockaddr_in *addr = (struct sockaddr_in*)&ai.addr;

        if( inet_pton( AF_INET, ai.sock_address, &addr->sin_addr.s_addr ) < 1)
        {
            throw std::invalid_argument("Bad given IP");
            return nullptr;
        }

        addr->sin_port        = htons( ai.port );
        addr->sin_family      = AF_INET;
        ai.ip_protocol        = AF_INET;
        ai.ai_addrlen         = sizeof(*addr);

        return &ai;
    }

    static inline addr_info*
    get_sockaddr_in6(addr_info& ai)
    {
        LOG(LOG_WARNING, "IPv6 is experimental", LOG_END);
        struct sockaddr_in6* addr = (struct sockaddr_in6*)&ai.addr;

        if( inet_pton(AF_INET6, ai.sock_address, &addr->sin6_addr) < 1)
        {
            throw std::invalid_argument("Given Ip address too short for IPv6");
            return nullptr;
        }

        addr->sin6_port  = htons(ai.port);
        ai.ai_addrlen    = sizeof(*addr);
        ai.ip_protocol   = AF_INET6;
        return &ai;
    }

    static inline addr_info*
    get_sockaddr_un(addr_info& ai)
    {
        struct sockaddr_un* addr = (struct sockaddr_un*)&ai.addr;

        ai.ip_protocol   = AF_UNIX;
      	strcpy( addr->sun_path, ai.sock_address);
        ai.ai_addrlen = SUN_LEN(addr);

        return  &ai;
    }

    static const int&
    fd(const Connection_T* src) noexcept
    {
        return src->fd;
    }

}; // class Tools

}   // Namespace ConnTools
}   // Namespace ForBy

#endif // FORBY_CONNTOOLS_TOOLS_INCLUDED

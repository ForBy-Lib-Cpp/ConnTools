#!/bin/bash

set -e

INSTALL_DIR=/usr/include/ForBy

echo ""
echo " ###############"
echo " >  ForBy 2018 "
echo ""
echo " ConnTools v0.3.1"
echo " ==> Downloading source code"

    git clone https://gitlab.com/ForBy-Lib-Cpp/ConnTools.git

echo " ==> Installing sources at /usr/include/ForBy" 

mkdir -p $INSTALL_DIR/ConnTools

cd ConnTools

cp ConnTools.hpp $INSTALL_DIR

cp -r core $INSTALL_DIR/ConnTools/
cp -r modules $INSTALL_DIR/ConnTools/

cd ..
rm -rf ConnTools

echo " ==> Done"

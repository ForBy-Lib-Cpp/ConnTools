//   ForBy LTD 2018
//   ConnTools

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef FORBY_CONNTOOLS
#define FORBY_CONNTOOLS

#define FORBY_CONNTOOLS_VERSION "0.3.1"

#include <ForBy/ConnTools/core/connection.h>
#include <ForBy/ConnTools/core/Socket.hpp>
#include <ForBy/ConnTools/core/SocketIO.hpp>
#include <ForBy/ConnTools/core/Tools.hpp>

#endif // FORBY_CONNTOOLS_SOCKET_T

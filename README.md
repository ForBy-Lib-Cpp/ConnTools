# ConnTools v0.3.1

**TESTING PENDING**

## Requirements:
  * **Logger** version >= 0.3  [ https://gitlab.com/ForBy-Lib-Cpp/Verbose/tree/master/Logger ]

## Compiler time
  *  Use -DDEFAULT_ADDRESS to change defaul address 127.0.0.1 / ::1
  *  Use -DDEFAULT_PORT to change default binding port ( current value is 0 )
  *  Use -DMAX_CONCURRENT to change default max concurrent listening

## Installation

  > curl https://gitlab.com/ForBy-Lib-Cpp/ConnTools/raw/master/install.sh | sudo bash -

## Changelog:

  * Socket IO model revised.
  * Added automatic socket configuration for common protocols
  * Added IPV6 Support
  * Fixed & reAdded UNIX Sockets support
  * Added STCP Support
  * Socket_T operator for direct access to Socket's FD
  * ( sendmsg/recvmsg integration under developvement )
 
### Experimental

  * Better event flow supporting C++React on ReactiveModule (Experimental)
  * Hierachy restructured as model v0.3f (See docs for further information)

## Documentation
  Refer to Project Wiki at Gitlab.
  
# Contact & Support

> jaskajokiranta@protonmail.com

# FURTHER IMPLEMENTATIONS ON v0.4

> Will require ForBy::Exception for C errno exception control support
> Etaprogramming technic implementacion

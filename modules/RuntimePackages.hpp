//  ForBy LTD 2017
/// ConnTools v0.3

#ifndef FORBY_CONNTOOLS_RUNTIMEPACKAGE
#define FORBY_CONNTOOLS_RUNTIMEPACKAGES

#include <string>
#include <atomic>
#include <stdexcept>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <arpa/inet.h>

#include <ForBy/ConnTools/core/Socket.hpp>

namespace ForBy{
namespace ConnTools
{

    struct Socket_Runtime
    {

	Socket_Runtime( const Socket_Runtime& _other) = delete;
	Socket_Runtime( Socket_Runtime&& _other) = delete;

	inline Socket_Runtime( const addrinfo* _addr)
        : addr( _addr )
	{}

        std::atomic<bool> in_use{false};
        std::atomic<bool> connected{false};
        std::atomic<bool> stopping{false};

	const addrinfo* addr;
    };


    struct Basic_Server_Runtime 
    : public Socket_Runtime
    {
        inline unsigned int left_session()           { left_sessions_++ ; return (total_sessions_ - left_sessions_); }  // Disconnected session
        inline unsigned int new_session()            {      total_sessions_++; return total_sessions_.load();        }  // New session
        inline unsigned int active_sessions()  const {         return (total_sessions_ - left_sessions_);            }  // Active session
        inline unsigned int total_sessions()   const {            return total_sessions_.load();                     }  // Sessions allover the runtime

    protected:

        std::atomic<unsigned int>  left_sessions_{0} ;   // Left Sessions
        std::atomic<unsigned int>  total_sessions_{0};   // Total count of sessions since startup
    };

} // namespace ConnTools
} // namespace ForBy

#endif // FORBY_CONNTOOLS_RUNTIMEPACKAGE

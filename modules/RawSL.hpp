//   ForBy LTD 2017
//   ConnTools v0.3
/// Raw Socket Layer

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CONNTOOLS_RAWSL_INCLUDED
#define CONNTOOLS_RAWSL_INCLUDED

#include <ForBy/ConnTools/core/SocketIO.hpp>

namespace ForBy{
namespace ConnTools
{

class RawSL
:   public SocketIO_T
{
public:

    ~RawSL(){}

    const char*
    meet( const char* _msg )
    {
        std::string cli_buff;
        try
        {
            __send( _msg );
            cli_buff = __recv();
        }
        catch( std::exception& e )
        {
             throw e;
             return NULL;
        }

        return cli_buff.c_str();
    }

    void
    sender( const char* _msg )
    {
      try
      {
          __send( _msg );
      }
      catch(std::exception& e)
      {
          throw std::runtime_error("A problem ocurred when sending a message");
          return;
      }
    }

    const char*
    receiver()
    {
        try
        {
           return __recv();
        }
        catch(std::runtime_error& e)
        {
            throw std::runtime_error("A problem ocurred when sending a message");
            return NULL;
        }
    }

};

} // Namespace ConnTools
} // Namespace ForBy

#endif // RAWSL_HPP_INCLUDED
